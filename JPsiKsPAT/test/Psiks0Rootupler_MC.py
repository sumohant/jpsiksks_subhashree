
import FWCore.ParameterSet.Config as cms
process = cms.Process("Rootuple")
#import os
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_38T_cff')
process.load('Configuration.StandardSequences.Reconstruction_cff')

process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff')
# from Configuration.AlCa.GlobalTag_condDBv2 import GlobalTag
# process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data')
#process.GlobalTag.globaltag = cms.string('102X_dataRun2_v12')
#process.GlobalTag.globaltag = cms.string('106X_mc2017_realistic_v8')
process.GlobalTag.globaltag = cms.string('106X_upgrade2018_realistic_v15_L1v1')

process.MessageLogger.cerr.FwkReport.reportEvery = 1000
process.options = cms.untracked.PSet( wantSummary = cms.untracked.bool(True) )
process.options.allowUnscheduled = cms.untracked.bool(True)
#process.load("FWCore.MessageLogger.MessageLogger_cfi")
process.maxEvents = cms.untracked.PSet(input = cms.untracked.int32(10000))
#process.maxEvents = cms.untracked.PSet(input = cms.untracked.int32(-1))


process.source = cms.Source("PoolSource",
    fileNames=cms.untracked.vstring(
#MiniAOD

## RE-REco
#"/store/mc/RunIIAutumn18MiniAOD/BsToJpsiKsKs_BMuonFilter_DGamma0_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/102X_upgrade2018_realistic_v15-v1/20000/136B1386-F74E-9247-AA07-8D0AA4E61C9C.root"
#"/store/mc/RunIIFall17MiniAODv2/BdToJpsiKsKs_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/100000/00C0BC2F-8895-E811-9D47-0CC47A78A41C.root"
#"/store/mc/RunIISummer19UL18MiniAOD/BdToJpsiKs_SoftQCDnonD_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/106X_upgrade2018_realistic_v11_L1v1-v1/10000/00108360-B706-3147-BEF0-36D6B48EA850.root",
#"/store/mc/RunIISummer19UL18MiniAOD/BdToJpsiKs_SoftQCDnonD_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/106X_upgrade2018_realistic_v11_L1v1-v1/10000/0765772C-471D-F443-A273-21DB12717D14.root",

"/store/mc/RunIIAutumn18MiniAOD/BsToJpsiKsKs_BMuonFilter_DGamma0_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/102X_upgrade2018_realistic_v15-v1/20000/136B1386-F74E-9247-AA07-8D0AA4E61C9C.root"
#'/store/mc/RunIIAutumn18MiniAOD/BsToJpsiKsKs_BMuonFilter_DGamma0_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/102X_upgrade2018_realistic_v15-v1/20000/2D12E609-37CE-5A4E-A9BD-47DA412C41A3.root',
#'/store/mc/RunIIAutumn18MiniAOD/BsToJpsiKsKs_BMuonFilter_DGamma0_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/102X_upgrade2018_realistic_v15-v1/20000/3ED5BB4A-CE6F-F444-BEC5-C71FDB0B3EB2.root',
#'/store/mc/RunIIAutumn18MiniAOD/BsToJpsiKsKs_BMuonFilter_DGamma0_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/102X_upgrade2018_realistic_v15-v1/20000/48399585-B950-8E40-BB58-B85CFB37B32E.root',
#'/store/mc/RunIIAutumn18MiniAOD/BsToJpsiKsKs_BMuonFilter_DGamma0_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/102X_upgrade2018_realistic_v15-v1/20000/544C7C0D-1D2D-3643-B816-748901BA6200.root'

#'/store/mc/RunIIFall17MiniAODv2/BdToJpsiKsKs_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/100000/00C0BC2F-8895-E811-9D47-0CC47A78A41C.root'
#'/store/mc/RunIIFall17MiniAODv2/BdToJpsiKsKs_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/100000/08B56A48-2E95-E811-8767-5065F381B211.root'
#'/store/mc/RunIIFall17MiniAODv2/BdToJpsiKsKs_TuneCP5_13TeV-pythia8-evtgen/MINIAODSIM/PU2017_12Apr2018_94X_mc2017_realistic_v14-v1/100000/12196C64-2F95-E811-8189-0025905A6132.root'

#data files
#'/store/data/Run2018A/Charmonium/MINIAOD/12Nov2019_UL2018_rsb-v1/230000/F6369E81-C308-594D-80D2-0628F98A3E77.root'
#'/store/data/Run2018C/Charmonium/MINIAOD/UL2018_MiniAODv2-v1/00000/02304131-2A08-594C-844E-EDB7070C2C3A.root' 
#'/store/data/Run2018C/Charmonium/MINIAOD/12Nov2019_UL2018-v1/00000/41EF51AA-8AF6-6448-84DE-6402EACC3547.root'
))        

process.load("slimmedMuonsTriggerMatcher_cfi")
process.load("Psiks0Rootupler_cfi")
#process.rootuple.OnlyGen = cms.bool(True)
process.rootuple.isMC = cms.bool(True)
process.TFileService = cms.Service("TFileService",
#                                fileName = cms.string(FILE2),#'Rootuple_Bdtojpiks0_2018MC_MiniAOD.root'),
                                fileName = cms.string('RootupleMC_BsJPsiKsKs.root')
)


process.p = cms.Path(process.slimmedMuonsWithTriggerSequence*process.rootuple)
#process.p = cms.Path(process.rootuple)

